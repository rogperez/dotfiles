# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

export DISABLE_AUTO_TITLE=true

ZSH_THEME="robbyrussell"

# User configuration
plugins=(rails ruby git)

# ============= Path {{
export PATH="\
/usr/local/bin:\
/bin:\
/sbin:\
/usr/bin:\
/usr/sbin:\
/usr/local/Cellar\
/usr/local/mysql/bin:\
/Users/rogerperez/bin:\
./node_modules/.bin:\
/Library/Frameworks/Python.framework/Versions/2.7/bin:\
/Applications/Postgres.app/Contents/Versions/9.4/bin"

source ~/.oh-my-zsh/oh-my-zsh.sh

# Yarn
export PATH="$PATH:`yarn global bin`"

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

# RVM
export PATH="$PATH:$HOME/.rvm/bin"
# }}

# Set vim bindings
bindkey -v
set editing-mode vi

export KEYTIMEOUT=1
export EDITOR=/usr/bin/vim

export BOOTSTRAPJS_ADM_URL='http://localhost:3001'
export BOOTSTRAPJS_INSIGHTS_URL='http://localhost:3002'
export BOOTSTRAPJS_SSO_URL='https://sso-beta.vervemobile.com'
export BOOTSTRAPJS_VLSM_URL='http://localhost:3004',
export BOOTSTRAPJS_REPORTS_URL='https://analytics-ui-qa.vervemobile.com'

source ~/.zshrc.d/aliases.bash
source ~/.zshrc.d/functions.bash
if [ -f ~/.secret.bash ]; then
  source ~/.secret.bash
fi

# ============= ZSH Options {{
# Don't record an entry that was just recorded again.
setopt HIST_IGNORE_DUPS
# Delete old recorded entry if new entry is a duplicate.
setopt HIST_IGNORE_ALL_DUPS
# }} 

# source z
if [ ! -f /usr/local/bin/z.sh ]; then
  pushd /usr/local/bin
  sudo curl -O https://raw.githubusercontent.com/rupa/z/master/z.sh
  sudo chmod 775 z.sh
  popd
fi

. /usr/local/bin/z.sh

# source fzf
if [ ! -f ~/.fzf.zsh ]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install --completion --key-bindings --no-update-rc
fi
. ~/.fzf.zsh
