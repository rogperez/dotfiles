"  =========================== Pathogen ========================  "
execute pathogen#infect()

" ==================== Color customization ====================  "
syntax on
filetype plugin indent on
syntax enable
set background=dark
colorscheme hybrid
"colorscheme xcode
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
let g:solarized_termcolors=256

highlight Search ctermfg=255 ctermbg=166

let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"

au BufNewFile,BufRead *.ejs set filetype=html
au BufNewFile,BufRead *.cap set filetype=ruby
au BufNewFile,BufRead Jenkinsfile set syntax=groovy

"  =======================Custom Settings ======================  "
let mapleader = ","
set clipboard=unnamed
if (match(system("uname"), "Linux") != -1)
  set clipboard=unnamedplus
endif
set undodir=~/.vim/.undo//
set backupdir=~/.vim/backup//
set directory=~/.vim/swp//
set conceallevel=0
set number
set laststatus=2
set hlsearch
set backspace=2
set tabstop=2
set shiftwidth=2
set expandtab
set nowrap
set wildmenu
set wildmode=full
set noswapfile
set ttimeoutlen=10

"  ------ Folding ------ "
set foldmethod=indent   " fold based on indent level
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
nnoremap <space> za
set foldlevelstart=5    " start with fold level of 1
autocmd Syntax c,cpp,vim,xml,html,xhtml setlocal foldmethod=syntax
autocmd Syntax c,cpp,vim,xml,html,xhtml,perl normal zR
autocmd BufWritePre * %s/\s\+$//e

function! PasteModeToggle()
  if(&paste == 0)
    set paste
  else
    set nopaste
  endif
endfunc
map <leader>pm :call PasteModeToggle()<cr>

"  ===================== Custom Bindings =======================  "
map <c-l> :tabn<cr>
map <c-h> :tabp<cr>
map <c-n> :tabnew<cr>
map <c-t> :NERDTreeToggle<cr>
map <c-n> :tabnew<cr>
map <c-q> :tabclose<cr>
map <c-w>r :so ~/.vimrc<cr>
map <c-w>_ :sp<cr>
map <c-w>\ :vs<cr>
map <c-w>N <plug>NERDTreeTabsToggle<cr>

" =========================== Silver Searcher =============================== "
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_user_caching = 0
  command! -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
  nnoremap \ :Ag<SPACE>
  let g:ackprg = 'ag --nogroup --nocolor --column'
  set grepprg=ag\ --nogroup\ --nocolor
endif

" ======================== Syntastic ============================ "
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_quiet_messages = {'regex': 'eslintignore|unused'}

" ======================== FZF ============================ "
set rtp+=~/.fzf
nmap <Leader>; :Buffers<CR>
nmap <c-p> :FZF<CR>
nmap <Leader>t :Files<CR>
nmap <Leader>r :Tags<CR>

" ======================== AIRLINE ============================ "
let g:airline_section_y = ''
let g:airline_section_z = ''
let g:airline_theme='dark'
"\'z'       : '#(uname)',
let g:tmuxline_preset = {
  \'a'       : '#S',
  \'win'     : '#I #W',
  \'cwin'    : '#I #W',
  \'x'       : '#(~/dotfiles/scripts/spotify.sh)',
  \'z'       : ['%a %D %l:%M %p'],
  \'options' : { 'status-justify' : 'left' }}


