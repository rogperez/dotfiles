#!/bin/bash
source /home/rogerperez/dotfiles/scripts/io.sh
source /home/rogerperez/dotfiles/scripts/set_interval.sh

read str
length=63

function scroll() {
  local res=""
  local str_length=${#str}

  local _char
  local _read_scroller=$(read_scroller)
  local _total_count=$( expr $_read_scroller + $length )

  local i=$(read_scroller)
  local j=0
  while (( "$i" < "$_total_count" )) ; do

    local _last_character_pointer=$(( $str_lengh - 1 ))
    if (( "$i" > "$_last_character_pointer" )) ; then
      j=$(( $i - $str_length ))
      _char=${str:$j:1}
    else
      _char=${str:$i:1}
    fi

    res="$res$_char"

    i=$(( $i + 1 ))
  done

  local max_overflow=$(( $str_length + $length - 1 ))
  if [ "$(read_scroller)" -eq "$max_overflow" ] ; then
    write_scroller $length
  else
    local pointer=$(( $(read_scroller) + 1 ))
    write_scroller $pointer
  fi

  echo $res
}

scroll
