#!/bin/bash

artist='$(osascript -e "tell application \"Spotify\" to artist of current track as string")'
track='$(osascript -e "tell application \"Spotify\" to name of current track as string")'
stat='$(osascript -e "tell application \"Spotify\" to player state as string")'

paused_status='paused'
playing_status='playing'
stopped_status='stopped'

full_command="if [[ $stat == $playing_status ]] ; then echo \[ $artist : $track \] ; else echo $stat ; fi"

if [[ "$OSTYPE" == "darwin"* ]]; then
  #/bin/bash -c "${full_command}"
else
  ssh rogerperez@192.168.90.1 $full_command
fi

