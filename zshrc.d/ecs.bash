function pi() {
  case $3 in
    "shell")
      ecs-shell $1 $2 $3 $4
    ;; "ssh")
      ecs-ssh $1 $2 $3
    ;; "chamber")
      chamber-cmd $1 $2 $3
    ;; *)
    echo "invalid command"
  esac
}

function ecs-shell() {
  case $1 in "production")
    region="us-east-1"
    ;; *)
    region="us-west-2"
  esac
  echo "AWS_DEFAULT_REGION=$region ecs-cmd $3 -c $1 -u ${4:-app} -s $2"
  AWS_DEFAULT_REGION=$region ecs-cmd $3 -c $1 -u ${4:-app} -s $2
}

function ecs-ssh() {
  case $1 in "production")
    region="us-east-1"
    ;; *)
    region="us-west-2"
  esac
  echo "AWS_DEFAULT_REGION=$region ecs-cmd $3 -c $1 -s $2"
  AWS_DEFAULT_REGION=$region ecs-cmd $3 -c $1 -s $2
}

function chamber-cmd() {
  case $1 in "foo")
    region="us-west-2"
    ;; *)
    region="us-east-1"
  esac
  echo "CHAMBER_AWS_REGION=$region chamber list -e $1_$2 | awk -v OFS='=\t' '{print $1, $6}'"
  #CHAMBER_AWS_REGION=$region chamber list -e "$1"_"$2" | awk -v OFS='=\t' '{print $1, $6}'
  CHAMBER_AWS_REGION=$region chamber list -e "$1"_"$2"
}
