function k8s() {
  # $1 = env: production
  # $2 = app: vlsm
  # $3 = service: webserver 
  kubectl exec -it $(kubectl get pods -o custom-columns=NAME:.metadata.name -l component=$1 | tail -n 1) "bash"
}
