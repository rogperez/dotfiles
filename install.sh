#! /bin/bash

INSTALL_SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR=$INSTALL_SOURCE_DIR

DEST_DIR=~
echo $DEST_DIR

echo "Linking Files into $DEST_DIR"
ln -s $DIR/vim $DEST_DIR/.vim
if [ -d $DEST_DIR/.ssh ]
then
  echo "~/.ssh directory already exists, attempting to synlink config file..."
  ln -s $DIR/ssh/config $DEST_DIR/.ssh/config
  echo "complete!"
else
  ln -s $DIR/ssh $DEST_DIR/.ssh
fi

ln -s $DIR/tmux.d $DEST_DIR/.tmux.d
ln -s $DIR/vimrc $DEST_DIR/.vimrc
ln -s $DIR/zshrc $DEST_DIR/.zshrc
ln -s $DIR/zshrc.d/ $DEST_DIR/.zshrc.d
ln -s $DIR/tmux/.tmux.conf $DEST_DIR/.tmux.conf
ln -s $DIR/tmuxinator $DEST_DIR/.tmuxinator

if [ -f /usr/bin/z.sh ]
then
  echo "z.sh found, skipping install"
else
  echo "Installing z.sh..."
  cd /usr/local/bin
  curl -O https://raw.githubusercontent.com/rupa/z/master/z.sh
  chmod 775 z.sh
  . /usr/local/bin/z.sh
  echo "complete!"
fi
